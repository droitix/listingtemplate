<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
   @include('layouts.partials._head')
 
</head>
<body>
    <div id="app">
   @include('layouts.partials._navigation')
       <div class="container">

         @include('layouts.partials._alerts')
        <main class="py-4">
            @yield('content')

        </div>
        </main>
    </div>

        <script src="/js/app.js"></script>
</body>
</html>
